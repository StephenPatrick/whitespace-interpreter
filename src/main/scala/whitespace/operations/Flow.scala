package whitespace.operations

import whitespace.operations.Stack.end_stack
import whitespace.operations.Stack.stack
import whitespace.operations.Stack.safe_pop
import whitespace.InterpreterUtils._
import whitespace.tokenizer.MoveInstruction

import scala.collection.mutable
import scala.util.Try


/*
****************************
* Flow functions
*
* These functions either label code or move the instruction pointer.
*/
object Flow {

  var call_stack = mutable.Stack[Array[Boolean]](end_stack)

  /**
   * Call a subroutine. This adds to the call stack a
   * pointer to the next instruction, and jumps to the label
   * given following this instruction.
   *
   * @param i_pointer The pointer to the current instruction
   * @return The new instruction pointer
   */
  def call_sub(loc: Int)(i_pointer :Int): Try[Int] =  {
    call_stack.push(itob(i_pointer))
    Try(loc)
  }

  /**
   * Return from a subroutine. Refers to the call stack
   * to determine the next execution point.
   *
   * Subroutines currently don't have their own stack management
   * I.e. ending or starting a sub won't change the stack pointer.
   * They are glorified labels with non-specific return jumps
   *
   * @return The new instruction pointer
   */
  val end_sub = () => Try(btoi_32(safe_pop(call_stack))) : Try[Int]

  /**
   * Jump unconditionally.
   *
   * @return The new instruction pointer
   */
  val jmp = (loc: Int) => () => Try(loc) : Try[Int]

  /**
   * Jump, given that the top element on the stack is 0.
   *
   * @param i_pointer The pointer to the current instruction
   * @return The new instruction pointer
   */
  def jez(loc: Int)(i_pointer :Int): Try[Int] = Try(if (btoi_32(safe_pop(stack)) == 0) loc else i_pointer)

  /**
   * Jump, given that the top element on the stack is less than 0.
   *
   * @param i_pointer The pointer to the current instruction
   * @return The new instruction pointer
   */
  def jlz(loc: Int)(i_pointer :Int): Try[Int] = Try(if (btoi_32(safe_pop(stack)) < 0) loc else i_pointer)

  /**
   * Reset the call stack to be empty.
   */
  def clear_call_stack() = call_stack = mutable.Stack[Array[Boolean]](end_stack)

}
