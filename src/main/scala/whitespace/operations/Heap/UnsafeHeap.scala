package whitespace.operations.Heap

class UnsafeHeap extends Heap {

  /**
   * The unsafe heap is a pseudo-ternary bit array.
   *
   * Bits can be true, false, or None.
   * None is used as a value terminator, as whitespace has no defined value size.
   */
  var unsafe_heap = new Array[Option[Boolean]](heap_size * 32)

  def heap_set(value : Array[Boolean], address : Int) = {
    for (i <- value.indices) {
      unsafe_heap(address+i) = Option(value(i))
    }
    unsafe_heap(address+value.length)= None
  }

  def heap_get(address : Int): Array[Boolean] = {
    var i = 0
    while (unsafe_heap(address+i).isDefined) i += 1
    unsafe_heap.slice(address,address+i-1).flatten
  }

  def clear_heap() = unsafe_heap = new Array[Option[Boolean]](heap_size * 32)
}
