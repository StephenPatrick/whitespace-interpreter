package whitespace.operations.Heap

import whitespace.InterpreterUtils._
import whitespace.operations.Stack._

import scala.util.Try


abstract class Heap {

  val heap_size = 256

  /**
   * Reset the heap to a blank state.
   */
  def clear_heap()

  /**
   * Implementation-specific placement of a value in a heap.
   */
  def heap_set(value : Array[Boolean], address : Int)

  /**
   * Implementation-specific retrieval from a value in a heap.
   */
  def heap_get(address : Int): Array[Boolean]

  /**
   * Take a $value and an $address off of the stack
   * (in that order)
   * and set the data at $address in the heap to $value.
   */
  val heap_store = () => heap_set(safe_pop(stack),btoi_32(safe_pop(stack)))

  /**
   * Take an $address off the stack and push onto the stack
   * the value stored at $address in the heap.
   */
  val heap_retrieve = () => stack.push(heap_get(btoi_32(safe_pop(stack)))) : Unit

}
