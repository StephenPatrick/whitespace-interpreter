package whitespace.operations.Heap

class SafeHeap extends Heap {

  /**
   * The safe heap implementation doesn't allow addresses to overlap.
   * In this sense, addresses are fake and are just indexes to arrays.
   */
  var heap = new Array[Array[Boolean]](heap_size)

  def heap_set(value : Array[Boolean], address : Int) = heap(address) = value
  def heap_get(address : Int): Array[Boolean] = heap(address)
  def clear_heap() = heap = new Array[Array[Boolean]](heap_size)

}
