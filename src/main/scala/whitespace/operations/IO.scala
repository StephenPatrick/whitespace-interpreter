package whitespace.operations

import whitespace.operations.Stack.stack
import whitespace.operations.Stack.safe_pop
import whitespace.Interpreter.heap_handler
import whitespace.InterpreterUtils._

import scala.collection.mutable
import scala.io.StdIn

object IO {

  /**
   * Print the value at the top of the stack as an integer.
   */
  val print_int = () => print(btoi_big(safe_pop(stack)).toString(10))

  /**
   * Print the value at the top of the stack as a character.
   */
  val print_char = () => print(btoi_32(safe_pop(stack)).toChar)

  /**
   * Read user input into the address given by the top of the stack.
   * (as an integer)
   */
  val read_int = () => read()

  /**
   * Read user input into the address given by the top of the stack.
   * (as a char)
   */
  val read_char = () => read()


  // We really don't care if it's an int or a char.
  // put whatever we were given on the heap.
  def read() = {
    val input = StdIn.readLine().toCharArray
    var out = new mutable.ArrayBuilder.ofBoolean()
    var input_pointer = 0
    while (input_pointer < input.length) {
      input(input_pointer) match {
        case 32 => out += false
        case 9 => out += true
      }
      input_pointer += 1
    }
    heap_handler.heap_set(out.result(),btoi_32(stack.top))
  }
}
