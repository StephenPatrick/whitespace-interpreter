package whitespace.operations

import scala.collection.mutable
import scala.util.Try

object Stack {

  val end_stack = new Array[Boolean](0)
  var stack = mutable.Stack[Array[Boolean]](end_stack)

  /**
   * Pop an item from a stack while confirming
   * we have not hit the end of the stack
   *
   * @param someStack A stack where the first pushed element was end_stack
   * @return The value popped
   */
  def safe_pop(someStack : mutable.Stack[Array[Boolean]]): Array[Boolean] = {
    val returnVal = someStack.pop()
    if (returnVal.sameElements(end_stack)) throw new Exception("Pop on end of stack")
    returnVal
  }

  /**
   * Push the following value onto the stack.
   *
   * @param value The value to push
   */
  val stack_push = (value : Array[Boolean]) => () => stack.push(value) : Unit

  /**
   * Duplicate the top of the stack.
   */
  val stack_dupe = () => stack.push(stack.top) : Unit

  /**
   * Swap the top two elements on the stack.
   */
  val stack_swap = () => {
    val num_1 = safe_pop(stack)
    val num_2 = safe_pop(stack)
    stack.push(num_1)
    stack.push(num_2)
  } : Unit

  /**
   * Discard the top of the stack.
   */
  val stack_discard = () => safe_pop(stack) : Unit

  /**
   * Reset the contents of the stack to contain nothing.
   */
  def clear_stack() = stack = mutable.Stack[Array[Boolean]](end_stack)
}
