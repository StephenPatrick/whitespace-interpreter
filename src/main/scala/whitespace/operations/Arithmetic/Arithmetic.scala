package whitespace.operations.Arithmetic

/**
 * This class defines basic arithmetic functions referenced during tokenization
 */
abstract class Arithmetic {
  def add = () => ()
  def sub = () => ()
  def mult = () => ()
  def div = () => ()
  def mod = () => ()
}