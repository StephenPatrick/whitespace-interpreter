package whitespace.operations.Arithmetic

import whitespace.InterpreterUtils._
import whitespace.operations.Stack._

import scala.util.Try

class BigArithmetic extends Arithmetic {

  def math_on_stack_big(math_fn: (BigInt,BigInt) => BigInt) = {
    val a = btoi_big(safe_pop(stack))
    val b = btoi_big(safe_pop(stack))
    stack.push(itob(math_fn(b,a)))
  } : Unit

  override val add = () => math_on_stack_big(_+_)
  override val sub = () => math_on_stack_big(_-_)
  override val mult = () => math_on_stack_big(_*_)
  override val div = () => math_on_stack_big(_/_)
  override val mod = () => math_on_stack_big(_%_)

  def btoi(b : Array[Boolean]): BigInt = whitespace.InterpreterUtils.btoi_big(b)
}
