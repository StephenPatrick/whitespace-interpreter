package whitespace.operations.Arithmetic

import whitespace.InterpreterUtils._
import whitespace.operations.Stack._

import scala.util.Try

class LongArithmetic extends Arithmetic {

  def math_on_stack_64(math_fn: (Long,Long) => Long) = {
    val a = btoi_64(safe_pop(stack))
    val b = btoi_64(safe_pop(stack))
    stack.push(itob(math_fn(b,a)))
  } : Unit

  override val add = () => math_on_stack_64(_+_)
  override val sub = () => math_on_stack_64(_-_)
  override val mult = () => math_on_stack_64(_*_)
  override val div = () => math_on_stack_64(_/_)
  override val mod = () => math_on_stack_64(_%_)

  def btoi(b : Array[Boolean]): Long = whitespace.InterpreterUtils.btoi_64(b)
}