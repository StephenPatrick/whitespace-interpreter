package whitespace.operations.Arithmetic

import whitespace.InterpreterUtils._
import whitespace.operations.Stack._

import scala.util.Try

class IntArithmetic extends Arithmetic {

  /**
   * Take two integers off the stack.
   * Apply the given math function to them.
   * Push the result onto the stack.
   *
   * @param math_fn Some math function (Int,Int) => Int
   * @return The new instruction pointer
   */
  def math_on_stack_32(math_fn: (Int, Int) => Int) = {
    val a = btoi_32(safe_pop(stack))
    val b = btoi_32(safe_pop(stack))
    stack.push(itob(math_fn(b, a)))
  } : Unit

  override val add = () => math_on_stack_32(_+_)
  override val sub = () => math_on_stack_32(_-_)
  override val mult = () => math_on_stack_32(_*_)
  override val div = () => math_on_stack_32(_/_)
  override val mod = () => math_on_stack_32(_%_)

  def btoi(b : Array[Boolean]): Int = whitespace.InterpreterUtils.btoi_32(b)
}
