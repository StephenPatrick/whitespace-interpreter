package whitespace

import scala.collection.mutable


object InterpreterUtils {

  /**
   * Convert a binary array into an integer
   *
   * @param x A boolean array
   * @return That array, converted into a 32 bit integer
   */
  def btoi_32(x : Array[Boolean]): Int = {
    // This implementation from Alnitak @StackOverflow
    var n = 0
    for (i <- x.indices) n = (n << 1) + (if (x(i)) 1 else 0)
    n
  }

  def btoi_64(x : Array[Boolean]): Long = {
    var n : Long = 0
    for (i <- x.indices) n = (n << 1) + (if (x(i)) 1 else 0)
    n
  }

  def btoi_big(x : Array[Boolean]): BigInt = {
    var n = BigInt(0)
    for (i <- x.indices) n = (n << 1) + (if (x(i)) 1 else 0)
    n
  }

  /**
   * Convert an integer into a binary array
   *
   * @param x Some integer
   * @return That integer, converted into a boolean array.
   */
  def itob(x : Int): Array[Boolean] = atob(x.toBinaryString.toCharArray)
  def itob(x : Long): Array[Boolean] = atob(x.toBinaryString.toCharArray)
  def itob(x : BigInt): Array[Boolean] = atob(x.toString(2).toCharArray)

  /**
   * Convert some array of 0s and 1s to an array of trues and falses
   * @param str An array of 0s and 1s
   * @return The input, with 0s as false and 1s as true
   */
  def atob(str : Array[Char]): Array[Boolean] = {
    val out = new mutable.ArrayBuilder.ofBoolean()
    for (i <- str.indices) out += (if (str(i) == '0') false else true)
    out.result()
  }
}