package whitespace

import whitespace.operations.Heap.{Heap,UnsafeHeap,SafeHeap}
import whitespace.Debug._
import whitespace.tokenizer._
import whitespace.operations.Arithmetic.{BigArithmetic, Arithmetic, IntArithmetic, LongArithmetic}
import whitespace.operations.Stack.clear_stack
import whitespace.operations.Flow.clear_call_stack


import scala.annotation.tailrec
import scala.collection.{immutable, mutable}
import scala.io.Source
import scala.util.{Failure, Success}

object Interpreter {

  val default_file = "conf/count.ws"
  var debug = false
  var instruction_memory = new Array[Instruction](0)
  var arithmetic_handler: Arithmetic = new IntArithmetic
  var heap_handler: Heap = new SafeHeap

  val flags = mutable.HashMap(
    "-nh" -> new Flag("Print no header", false,
              print_header),
    "-d" -> new Flag("Debug mode", false,
              () => {debug = true}),
    "-unsafe" -> new Flag("Use real heap structure", false,
              () => {heap_handler = new UnsafeHeap}),
    "-int" -> new Flag("(Default) Use 32-bit numbers", true,
              () => {arithmetic_handler = new IntArithmetic}),
    "-long" -> new Flag("Use 64-bit numbers", false,
              () => {arithmetic_handler = new LongArithmetic}),
    "-bigint" -> new Flag("Use unlimited-bit numbers", false,
              () => {arithmetic_handler = new BigArithmetic})
  )

  /**
   * Command line entry point
   *
   * @param args File name + series of flags
   */
  def main(args : Array[String]): Unit = {
    var file = default_file
    if (args.length < 1) {
      print_usage_details(file)
    } else {
      file = args(0)
      set_flags(args.drop(1))
    }
    check_flags()
    run(file)
  }

  /**
   * New instruction entry point
   *
   * @param i_pointer The index of the instruction to run from instruction memory
   */
  @tailrec
  def eval(i_pointer: Int): Unit = {
    if (debug) print_eval_debug(i_pointer)
    instruction_memory(i_pointer) match {
      case ConditionalInstruction(fn) => fn(i_pointer+1) match {
        case Success(pointer) => eval(pointer)
        case Failure(ex) => throw new Exception("Failure at character " + i_pointer + ": " + ex.getMessage)
      }
      case MoveInstruction(fn) => fn() match {
        case Success(pointer) => if (pointer != -1) eval(pointer)
        case Failure(ex) => throw new Exception("Failure at character " + i_pointer + ": " + ex.getMessage)
      }
      case BareInstruction(fn) => fn(); eval(i_pointer+1)
    }
  }

  /**
   * Tokenize and run a file, cleaning up after.
   *
   * @param file The file to run.
   */
  def run(file : String) = {
    // Work is duplicated here if debug is on.
    // Considering speed probably isn't a concern when debugging, this should be acceptable.
    val t = new Tokenizer(heap_handler, arithmetic_handler)
    if (debug) (new DebugTokenizer).tokenize(Source.fromFile(file).mkString)
    t.tokenize(Source.fromFile(file).mkString) match {
      case Success(tokens) =>
        instruction_memory = tokens
        eval(0)
        cleanup()
      case Failure(ex) => throw ex
    }
  }

  /**
   * Clear heap and stack spaces for following runs
   */
  def cleanup() = {
    heap_handler.clear_heap()
    clear_stack()
    clear_call_stack()
  }

  def test() = {
    //run("conf/count.ws")
    //run("conf/name.ws")
    run("conf/hworld.ws")
    //run("conf/calc.ws")
    //run("conf/fact.ws")
    //run("conf/fibonnaci.ws")
    //run("conf/hanoi.ws")
    //run("conf/sudoku.ws")
  }

  /**
   * Parse command line arguments and set runtime flags
   *
   * @param args Command line arguments
   */
  def set_flags(args : Array[String]) = {
    for (i <- args.indices) {
      flags get args(i) match {
        case Some(flag) => flag.activate()
        case None => throw new Exception("Unknown flag " + args(i))
      }
    }
  }

  /**
   * Trigger any flags which are active
   */
  def check_flags() = {
    for (flag <- flags.values) {
      if (flag.active) flag.trigger()
    }
  }
}