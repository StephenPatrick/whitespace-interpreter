package whitespace.tokenizer

import whitespace.InterpreterUtils._
import TokenUtils._

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.{Failure, Success, Try}

class DebugTokenizer {

  var instructions = Array[Char](0)

  def tokenize(s : String) = {
    instructions = s.toCharArray.filter(strip_comments)
    print(parse_token(0, ""))
  }

  @tailrec
  final def parse_token(i_pointer : Int, result : String): String = {
    if (i_pointer >= instructions.length) return result
    val (next_token, new_pointer) = instructions(i_pointer) match {
      case `space` => parse_stack(i_pointer + 1)
      case `tab` => parse_tab(i_pointer + 1)
      case `linefeed` => parse_flow(i_pointer + 1)
      case `hash` => multiline_comment(i_pointer + 1)
    }
    next_token match {
      case Success(token : String) =>
        parse_token(new_pointer, result + token)
      case Failure(ex) => throw new Exception("Failure tokenizing character " +
        i_pointer + ": " + ex.getMessage)
    }
  }

  def multiline_comment(i_pointer : Int): (Try[String], Int) = {
    var new_pointer = i_pointer
    while (new_pointer < instructions.length && instructions(new_pointer) != `hash`){
      new_pointer += 1
    }
    if (new_pointer >= instructions.length) {
      throw new Exception("Unmatched hash for multiline comment")
    }
    instructions(new_pointer) match {
      case `space` => parse_stack(i_pointer + 1)
      case `tab` => parse_tab(i_pointer + 1)
      case `linefeed` => parse_flow(i_pointer + 1)
    }
  }

  //  Character Combination (S_pace, T_ab, L_inefeed, V_alue*) - Name - Shortname
  //                         *Values are a string of S and T with an L terminator
  //  SSV  - Stack Push - PUSH
  //  SLS  - Stack Dupe - DUPE
  //  SLT  - Stack Swap - SWAP
  //  SLL  - Stack Discard - DROP
  //  TSSS - Addition - ADD
  //  TSST - Subtraction - SUB
  //  TSSL - Multiplication - MULT
  //  TSTS - Division - DIV
  //  TSTT - Modulo - MOD
  //  TTS  - Heap Store - STOR
  //  TTT  - Heap Retrieve - GET
  //  TLSS - Print top stack char - PCHR
  //  TLST - Print top stack int - PNUM
  //  TLTS - Read a char into heap - RCHR
  //  TLTT - Read an int into heap - RNUM
  //  LSSV - Mark Label - LABL
  //  LSTV - Call Subroutine - CALL
  //  LSLV - JMP
  //  LTSV - JEZ
  //  LTTV - JLZ
  //  LTL  - End Subroutine - RTRN
  //  LLL  - End Program - STOP

  //**** [Space]
  def parse_stack(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => make_value_pair("PUSH",i_pointer + 1)
    case `tab` => throw new Exception("[Space][Tab] is not a known instruction")
    case `linefeed` => parse_stack_manip(i_pointer + 1)
  }

  //******** [Space][LF]
  def parse_stack_manip(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => make_instruction("DUPE", i_pointer + 1)
    case `tab` => make_instruction("SWAP", i_pointer + 1)
    case `linefeed` => make_instruction("DROP", i_pointer + 1)
  }

  //**** [Tab]
  def parse_tab(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => parse_arith(i_pointer + 1)
    case `tab` => parse_heap(i_pointer + 1)
    case `linefeed` => parse_io(i_pointer + 1)
  }

  //******** [Tab][Space]
  def parse_arith(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => parse_arith_basic(i_pointer + 1)
    case `tab` => parse_arith_adv(i_pointer + 1)
    case `linefeed` => throw new Exception("[Tab][Space][LF] is not a known instruction")
  }

  //************ [Tab][Space][Space]
  def parse_arith_basic(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => make_instruction("ADD", i_pointer + 1)
    case `tab` => make_instruction("SUB", i_pointer + 1)
    case `linefeed` => make_instruction("MULT", i_pointer + 1)
  }

  //************ [Tab][Space][Tab]
  def parse_arith_adv(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => make_instruction("DIV", i_pointer + 1)
    case `tab` => make_instruction("MOD", i_pointer + 1)
    case `linefeed` => throw new Exception("[Tab][Space][Tab][LF] is not a known instruction")
  }

  //******** [Tab][Tab]
  def parse_heap(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => make_instruction("STOR", i_pointer + 1)
    case `tab` => make_instruction("GET", i_pointer + 1)
    case `linefeed` => throw new Exception("[Tab][Tab][LF] is not a known instruction")
  }

  //******** [Tab][LF]
  def parse_io(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => parse_print(i_pointer + 1)
    case `tab` => parse_read(i_pointer + 1)
    case `linefeed` => throw new Exception("[Tab][LF][LF] is not a known instruction")
  }

  //************ [Tab][LF][Space]
  def parse_print(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => make_instruction("PCHR", i_pointer + 1)
    case `tab` => make_instruction("PNUM", i_pointer + 1)
    case `linefeed` => throw new Exception("[Tab][LF][Space][LF] is not a known instruction")
  }

  //************ [Tab][LF][Tab]
  def parse_read(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => make_instruction("RCHR", i_pointer + 1)
    case `tab` => make_instruction("RNUM", i_pointer + 1)
    case `linefeed` => throw new Exception("[Tab][LF][Tab][LF] is not a known instruction")
  }

  //**** [LF]
  def parse_flow(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => parse_flow_uncond(i_pointer + 1)
    case `tab` => parse_flow_cond(i_pointer + 1)
    case `linefeed` => parse_end_program(i_pointer + 1)
  }

  //******** [LF][SPACE]
  def parse_flow_uncond(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => make_value_pair("LABL",i_pointer + 1)
    case `tab` => make_value_pair("CALL",i_pointer + 1)
    case `linefeed` => make_value_pair("JMP",i_pointer + 1)
  }

  //******** [LF][TAB]
  def parse_flow_cond(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `space` => make_value_pair("JEZ",i_pointer + 1)
    case `tab` => make_value_pair("JLZ",i_pointer + 1)
    case `linefeed` => make_instruction("RTRN", i_pointer + 1)
  }

  //******** [LF][LF]
  def parse_end_program(i_pointer: Int): (Try[String], Int) = instructions(i_pointer) match {
    case `linefeed` => make_instruction("QUIT", i_pointer + 1)
    case _ => throw new Exception("[LF][LF] not followed by [LF] -- improper program termination")
  }

  /**
   * Read a value from code.
   *
   * @param i_pointer The current instruction pointer
   * @return The value read, tokenized
   * @return The updated instruction pointer
   */
  def parse_val(i_pointer : Int): (String, Int) = {
    var out = new mutable.ArrayBuilder.ofBoolean()
    var reading = true
    var new_pointer = i_pointer
    while (reading) {
      instructions(new_pointer) match {
        case 32 => out += false
        case 9 => out += true
        case 10 => reading = false
      }
      new_pointer += 1
    }
    (btoi_32(out.result()).toString, new_pointer)
  }

  def make_instruction(fn : String, i_pointer : Int): (Try[String], Int) = {
    (Try(fn + "\n"), i_pointer)
  }

  def make_value_pair(fn : String, i_pointer : Int): (Try[String], Int) = {
    val (next_value, new_pointer) = parse_val(i_pointer)
    (Try(fn + ":" + next_value + "\n"), new_pointer)
  }
}