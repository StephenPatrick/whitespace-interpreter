package whitespace.tokenizer

import scala.util.Try

/**
 * A token parsed from whitespace code.
 */
abstract sealed class Token {
  def mkString: String = this.getClass.getName
}

/**
 * A basic instruction that takes no parameters
 * (Or has parameters curried)
 */
abstract sealed class Instruction() extends Token

case class BareInstruction(fn : () => Unit) extends Instruction
case class ConditionalInstruction(fn : (Int) => Try[Int]) extends Instruction
case class MoveInstruction(fn : () => Try[Int]) extends Instruction

/**
 * An instruction, used in tokenization exclusively, representing a jump.
 * Converted to an Instruction before evaluation.
 */
abstract sealed class Jump() extends Token

case class ConditionalJump(fn : (Int) => (Int) => Try[Int], value: Array[Boolean]) extends Jump
case class UnconditionalJump(fn : (Int) => () => Try[Int], value : Array[Boolean]) extends Jump

/**
 * A token representing an instruction which makes a label.
 * The instruction is removed and later references to said label
 * are replaced with the index following the instruction.
 *
 * Used in tokenization exclusively.
 *
 * @param name The label name to replace with an index
 */
case class Label(name : Array[Boolean]) extends Token