package whitespace.tokenizer

object TokenUtils {

  val space = 32
  val tab = 9
  val linefeed = 10
  val hash = 35

  // Comments in whitespace are any
  // characters that are not whitespace
  //
  // Use hash marks (35) to signify beginning and
  // ending of multiline comments.
  def strip_comments(c : Char): Boolean = {
    c == space || c == tab || c == linefeed || c == hash
  }

  def to_instruction(token : Token): Instruction = token.asInstanceOf[Instruction]
}
