package whitespace.tokenizer

import whitespace.operations.Arithmetic.Arithmetic
import whitespace.operations.Flow._
import whitespace.operations.Heap.Heap
import whitespace.operations.IO._
import whitespace.operations.Stack._
import whitespace.InterpreterUtils._
import TokenUtils._

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.{Failure, Success, Try}

class Tokenizer(heap_handler : Heap, arithmetic_handler : Arithmetic) {

  // Whitespace Code String
  var code = Array[Char](0)
  // [Label, Instruction Index to Jump to]
  var labels = new mutable.HashMap[Long, Int]
  // [Instruction Index of Call, Label]
  var jump_calls = new mutable.HashMap[Int, Long]

  /**
   * Take a string of whitespace and parse it as a series of instructions
   * 
   * @param s A code string
   * @return The instructions that that string represents
   */
  def tokenize(s : String): Try[Array[Instruction]] = {
    code = s.toCharArray.filter(strip_comments)
    resolve_labels(parse_token(0, new Array[Token](0)))
  }

  /**
   * Recursively parse a token from code
   *
   * @param c_pointer The current code pointer
   * @param old_tokens The current list of tokens
   * @return The completed list of tokens
   */
  @tailrec
  final def parse_token(c_pointer : Int, old_tokens : Array[Token]): Array[Token] = {
    if (c_pointer >= code.length) return old_tokens
    val (next_token, new_pointer) = code(c_pointer) match {
      case `space` => parse_stack(c_pointer + 1)
      case `tab` => parse_tab(c_pointer + 1)
      case `linefeed` => parse_flow(c_pointer + 1)
      case `hash` => multiline_comment(c_pointer + 1)
    }
    next_token match {
      case Success(token : Token) =>
        token match {
          case Label(name) =>

            // Get rid of all mark_label instructions
            // Mark said labels in the tokenizer later.
            labels += (btoi_64(name) -> old_tokens.length)
            parse_token(new_pointer, old_tokens)

          case UnconditionalJump(_, label) =>

            // On a jump call, record where the jump call was made
            // So it's label can be replaced with a real index later.
            jump_calls += (old_tokens.length -> btoi_64(label))
            parse_token(new_pointer, old_tokens :+ token)

          case ConditionalJump(_, label) =>

            jump_calls += (old_tokens.length -> btoi_64(label))
            parse_token(new_pointer, old_tokens :+ token)

          case _ => parse_token(new_pointer, old_tokens :+ token)
        }
      case Failure(ex) =>
        throw new Exception("Failure tokenizing character " + c_pointer + ": " + ex.getMessage)
    }
  }

  /**
   * Pause parsing until a second hash is seen
   *
   * @param c_pointer The current code pointer
   * @return The operation following the multiline comment
   * @return The new code pointer
   */
  def multiline_comment(c_pointer : Int): (Try[Token], Int) = {
    var new_pointer = c_pointer
    while (new_pointer < code.length && code(new_pointer) != `hash`){
      new_pointer += 1
    }
    if (new_pointer >= code.length) {
      throw new Exception("Unmatched hash for multiline comment")
    }
    code(new_pointer) match {
      case `space` => parse_stack(c_pointer + 1)
      case `tab` => parse_tab(c_pointer + 1)
      case `linefeed` => parse_flow(c_pointer + 1)
    }
  }

  /**
   * Find all labels in a token array and replace them with
   * instruction indices. Return an array of Instructions.
   *
   * @param tokens A token array with jumps to labels
   * @return The same array with all jumps pointing to instruction indices
   */
  def resolve_labels(tokens : Array[Token]): Try[Array[Instruction]] = {
    jump_calls.par.foreach({
        case (jump_index, label) =>
          labels get label match {
            case Some(label_index : Int) =>
              tokens(jump_index) match {
                case ConditionalJump(fn, value) =>
                  tokens(jump_index) = ConditionalInstruction(fn(label_index))
                case UnconditionalJump(fn, value) =>
                  tokens(jump_index) = MoveInstruction(fn(label_index))
                case _ => throw new Exception("No jump instruction at index: " + jump_index)
              }
            case None => throw new Exception("Label " + label + " not found.")
          }
    })
    // This mapping just re-labels the array to Array[Instruction]
    // it already contains (or should contain) exclusively Instructions
    Try(tokens.map(to_instruction))
  }

  /**
   * Parse a given pattern of characters into an operation
   *
   * @param c_pointer The current code pointer
   * @return An operation
   * @return The updated code pointer
   */

  //**** [Space]
  def parse_stack(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` =>
      val (next_value, new_pointer) = parse_val(c_pointer + 1)
      (Try(BareInstruction(stack_push(next_value))), new_pointer)
    case `tab` => throw new Exception("[Space][Tab] is not a known instruction")
    case `linefeed` => parse_stack_manip(c_pointer + 1)
  }

  //******** [Space][LF]
  def parse_stack_manip(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => (Try(BareInstruction(stack_dupe)), c_pointer + 1)
    case `tab` => (Try(BareInstruction(stack_swap)), c_pointer + 1)
    case `linefeed` => (Try(BareInstruction(stack_discard)), c_pointer + 1)
  }

  //**** [Tab]
  def parse_tab(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => parse_arith(c_pointer + 1)
    case `tab` => parse_heap(c_pointer + 1)
    case `linefeed` => parse_io(c_pointer + 1)
  }

  //******** [Tab][Space]
  def parse_arith(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => parse_arith_basic(c_pointer + 1)
    case `tab` => parse_arith_adv(c_pointer + 1)
    case `linefeed` => throw new Exception("[Tab][Space][LF] is not a known instruction")
  }

  //************ [Tab][Space][Space]
  def parse_arith_basic(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => (Try(BareInstruction(arithmetic_handler.add)), c_pointer + 1)
    case `tab` => (Try(BareInstruction(arithmetic_handler.sub)), c_pointer + 1)
    case `linefeed` => (Try(BareInstruction(arithmetic_handler.mult)), c_pointer + 1)
  }

  //************ [Tab][Space][Tab]
  def parse_arith_adv(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => (Try(BareInstruction(arithmetic_handler.div)), c_pointer + 1)
    case `tab` => (Try(BareInstruction(arithmetic_handler.mod)), c_pointer + 1)
    case `linefeed` => throw new Exception("[Tab][Space][Tab][LF] is not a known instruction")
  }

  //******** [Tab][Tab]
  def parse_heap(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => (Try(BareInstruction(heap_handler.heap_store)), c_pointer+1)
    case `tab` => (Try(BareInstruction(heap_handler.heap_retrieve)), c_pointer+1)
    case `linefeed` => throw new Exception("[Tab][Tab][LF] is not a known instruction")
  }

  //******** [Tab][LF]
  def parse_io(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => parse_print(c_pointer + 1)
    case `tab` => parse_read(c_pointer + 1)
    case `linefeed` => throw new Exception("[Tab][LF][LF] is not a known instruction")
  }

  //************ [Tab][LF][Space]
  def parse_print(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => (Try(BareInstruction(print_char)), c_pointer+1)
    case `tab` => (Try(BareInstruction(print_int)), c_pointer+1)
    case `linefeed` => throw new Exception("[Tab][LF][Space][LF] is not a known instruction")
  }

  //************ [Tab][LF][Tab]
  def parse_read(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => (Try(BareInstruction(read_char)), c_pointer+1)
    case `tab` => (Try(BareInstruction(read_int)), c_pointer+1)
    case `linefeed` => throw new Exception("[Tab][LF][Tab][LF] is not a known instruction")
  }

  //**** [LF]
  def parse_flow(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => parse_flow_uncond(c_pointer + 1)
    case `tab` => parse_flow_cond(c_pointer + 1)
    case `linefeed` => parse_end_program(c_pointer + 1)
  }

  //******** [LF][SPACE]
  def parse_flow_uncond(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` =>
      val (value, new_pointer) = parse_val(c_pointer)
      (Try(Label(value)), new_pointer)
    case `tab` => make_jump_call(call_sub,c_pointer + 1)
    case `linefeed` =>
      val (next_value, new_pointer) = parse_val(c_pointer+1)
      (Try(UnconditionalJump(jmp,next_value)), new_pointer)
  }

  //******** [LF][TAB]
  def parse_flow_cond(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `space` => make_jump_call(jez,c_pointer + 1)
    case `tab` => make_jump_call(jlz,c_pointer + 1)
    case `linefeed` => (Try(MoveInstruction(end_sub)), c_pointer+1)
  }

  //******** [LF][LF]
  def parse_end_program(c_pointer: Int): (Try[Token], Int) = code(c_pointer) match {
    case `linefeed` => (Try(MoveInstruction(()=>Try(-1))), c_pointer + 1)
    case _ => throw new Exception("[LF][LF] not followed by [LF] -- improper program termination")
  }

  /**
   * Read a value from code.
   *
   * @param c_pointer The current code pointer
   * @return The value read, tokenized
   * @return The updated code pointer
   */
  def parse_val(c_pointer : Int): (Array[Boolean], Int) = {
    var out = new mutable.ArrayBuilder.ofBoolean()
    var reading = true
    var new_pointer = c_pointer
    while (reading) {
      code(new_pointer) match {
        case 32 => out += false
        case 9 => out += true
        case 10 => reading = false
      }
      new_pointer += 1
    }
    (out.result(), new_pointer)
  }

  /**
   * Construct a conditional jump call
   *
   * @param fn The jump operation
   * @param c_pointer the current code pointer
   * @return A conditional jump call with the label given from the instruction pointer.
   * @return The updated code pointer
   */
  def make_jump_call(fn : (Int) => (Int) => Try[Int], c_pointer: Int): (Try[Token], Int) = {
    val (next_value, new_pointer) = parse_val(c_pointer)
    (Try(ConditionalJump(fn,next_value)), new_pointer)
  }
}