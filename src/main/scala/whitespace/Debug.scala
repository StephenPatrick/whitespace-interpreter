package whitespace

import whitespace.operations.Stack.stack
import whitespace.InterpreterUtils.btoi_32
import whitespace.Interpreter.flags

object Debug {

  /**
   * Print 'whitespace interpreter'
   */
  def print_header () = {
    println("+-------------------------------+")
    println("|                               |")
    println("|                interpreter    |")
    println("+-------------------------------+")
  }

  /**
   * Print the current instruction pointer and the
   * contents of the stack.
   *
   * @param i_pointer The current instruction pointer
   */
  def print_eval_debug (i_pointer : Int) = {
    println("")
    print("Instruction: " + i_pointer)
    print(" Stack:")
    var print_iter = 1
    for (elem <- stack.toList) {
      if (print_iter == 0) println("       ")
      print(btoi_32(elem)+",")
      print_iter = (print_iter + 1) % 5
    }
    Thread.sleep(50)
  }

  /**
   * Print flags and usage notes
   *
   * @param file The name of the default file
   */
  def print_usage_details (file : String) = {
    println("Usage: java -jar whitespace.jar file.ws")
    println("Flags:")
    for ((key, flag) <- flags) {
      println("    " + key + ": " + flag.get_details)
    }
    println("Running " + file + " by default")
    println("")
  }
}
