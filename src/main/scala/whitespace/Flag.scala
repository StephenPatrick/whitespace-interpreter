package whitespace

class Flag(details : String, var active: Boolean, fn: () => Unit) {
  def activate() {active = true}
  def trigger() {fn()}
  def get_details: String = details
}
